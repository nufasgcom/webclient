    EMPTY= 'Empty'
    webSocket = {}
    charData = {
        labels: [],
        datasets: [{
             
        }]
    }
    const chartConfig = {
      type: 'line',
      data: charData,
      options: {
        responsive: true,
        plugins: {
          legend: {
            position: 'top',
            labels: {
                font: {
                    size: 22
                }

            }
          },
          title: {
            display: false,
            text: 'Chart'
          }
        }
      },
    };
    
    var chartCOUNT=10
    var popCanvas = []
    var myChart = []

    
    window.onload = function() { // Начальные действия
    
         for (i=0; i<chartCOUNT; i++) {
            let div = document.createElement('div');
            div.className = 'template chart';
            div.id = 'chart'+i;
            div.setAttribute('chartID',i);
            html = "<canvas class='chartCanvas' id='chartCanvas"+i+"'></canvas>";
            div.innerHTML = html;
            table = document.getElementById("table_div");
            table.after(div);
            divid = "chartCanvas"+i;
            popCanvas.push(document.getElementById(divid).getContext("2d"));
            myChart.push(new Chart(popCanvas[popCanvas.length-1], chartConfig));
        }
        forHide = document.getElementById('loading');
        forHide.hidden = true;
        forShow = document.getElementById('interface');
        forShow.hidden = false;
        ConnectToWebSocket(connect.getAttribute('url'))
        if (!LoadCmdsFromCookie()) {
            add_subscribe_click();
        }
        disable_cmd_input();
        elem = document.getElementById('topics');
        elem.atopics = [];
        add_to_topics('*');
//        var mychart = new Chart(popCanvas, chartConfig);

    }
    
    function topicChange(elem){
        filter = elem.value;
        filter_ = filter.replaceAll('*','\\w*');
        regstr = '^'+filter_+'$';
        re = new RegExp(regstr);
        trs = document.querySelectorAll('tbody tr');
        for (item of trs) {
            itemTopic = item.querySelector('.td2');
            if (itemTopic.innerHTML.includes(filter) || itemTopic.innerHTML.search(re)>=0) {
                if (item.classList.contains('hidden')) {
                    item.classList.remove('hidden');
                }
            } else {
                item.classList.add('hidden');
            }
        }
        if ((filter.trim() == '') || (filter.trim() == '*')) {
            clearFilter.classList.add('hidden');
            elem.classList.remove('btn-danger');
        } else {
            clearFilter.classList.remove('hidden');
            elem.classList.add('btn-danger');
        }
    }
    
    function clearTopicFilter(elem) {
        filter = document.querySelector('.td2 input');
        filter.value = '';
        topicChange(filter);
    }

    function LoadCmdsFromCookie(){
        result = false;
        str = getCookie('topics');
        if (str) {
            topics = str.split('{|}');
            str = getCookie('msgs');
            if (str) {
                msgs = str.split('{|}');

                for (i in topics) {
                    if (i>1) {
                        add_subscribe_click();
                        elements = document.querySelectorAll('input.topic');
                        elements[i-1].value = topics[i];
                        elements = document.querySelectorAll('input.msg');
                        elements[i-1].value = msgs[i];
                        result =true;
                    }
                }
            }
        }
        return result;
    }

    function ConnectToWebSocket(url){
        printToTerminal('Attempt connect to WebSocket by url:'+WEB_SOCKET_URL)
        webSocket = createSocket(WEB_SOCKET_URL);// определение переменной в terminal.html
    }

    function terminal_clear_click(elem){ // Обработчик очистки терминала
        terminal.value='';
    }

    function add_subscribe_click(elem){ // Обработчик добавления командной строки
        divCmds = document.querySelectorAll('div.cmd_div');
        divCmd = divCmds[0];
        div = document.createElement('div');
        div.className = 'cmd_div';
        div.innerHTML = divCmd.innerHTML;
        divCmds[divCmds.length-1].after(div);
        elements = document.body.querySelectorAll('button.del_subscribe_btn');
        if (elements.length > 2) {
            for (el of elements) {
                el.hidden = false;
                el.removeAttribute('disabled');
            }
        }
    }

    function del_subscribe_click(elem){ // Обработчик удаления подписки
        divCmds = document.querySelectorAll('div.cmd_div');
        if (divCmds.length > 1) { // Смотрим что бы оставалась одна командная строка
            divCmd = elem.parentElement;
            unsubscribe(elem);
            divCmd.remove();
            if (divCmds.length == 3) {
                els = document.body.querySelectorAll('button.del_subscribe_btn');
                for (el of els) {
                    el.hidden = true;
                }
            }
        }
    }

    function setEmptyErrClass(elem) { // если значение элемента равно "" добавить класс EMPTY
        if  (!!elem) {
            if (elem.value.trim() == '') {
                elem.classList.add(EMPTY)
            } else {
                elem.classList.remove(EMPTY)
            }
        }
    }



    function getTopic(elem) { // Получаем элемент ввода команд по соседнему элементу
        el=getSister(elem,'input.topic');
        setEmptyErrClass(el)
        return el;
    }

    function CreateRequest()
    {
        var Request = false;

        if (window.XMLHttpRequest)
        {
            //Gecko-совместимые браузеры, Safari, Konqueror
            Request = new XMLHttpRequest();
        }
        else if (window.ActiveXObject)
        {
            //Internet explorer
            try
            {
                 Request = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (CatchException)
            {
                 Request = new ActiveXObject("Msxml2.XMLHTTP");
            }
        }
        if (!Request)
        {
            console.log("Невозможно создать XMLHttpRequest");
        }
        return Request;
    }

        /*
    Функция посылки запроса к файлу на сервере
    r_method  - тип запроса: GET или POST
    r_path    - путь к файлу
    r_args    - аргументы вида a=1&b=2&c=3...
    r_handler - функция-обработчик ответа от сервера
    */
    function SendRequest(r_method, r_path, r_args, r_handler)
    {
        //Создаём запрос
        var Request = CreateRequest();

        //Проверяем существование запроса еще раз
        if (!Request)
        {
            return;
        }

        //Назначаем пользовательский обработчик
        Request.onreadystatechange = function()
        {
            //Если обмен данными завершен
            if (Request.readyState == 4)
            {
                //Передаем управление обработчику пользователя
                r_handler(Request);
            }
        }

        //Проверяем, если требуется сделать GET-запрос
        if (r_method.toLowerCase() == "get" && r_args.length > 0)
        r_path += "?" + r_args;

        //Инициализируем соединение
        Request.open(r_method, r_path, true);

        if (r_method.toLowerCase() == "post")
        {
            //Если это POST-запрос

            //Устанавливаем заголовок
            Request.setRequestHeader("Content-Type","application/x-www-form-urlencoded; charset=utf-8");
            //Посылаем запрос
            Request.send(r_args);
        }
        else
        {
            //Если это GET-запрос

            //Посылаем нуль-запрос
            Request.send(null);
        }
    }

    // возвращает куки с указанным name,
    // или undefined, если ничего не найдено
    function getCookie(name) {
      let matches = document.cookie.match(new RegExp(
        "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
      ));
      return matches ? decodeURIComponent(matches[1]) : undefined;
    }

    function SendToDB(text){ // Ассинхронная отправка сообщения на сервер для записи в БД
        var Handler = function (Request) {
            console.log(Request.responseText);
        }
        csrf_token = getCookie('csrftoken');
        arg = 'text='+text+'&csrfmiddlewaretoken='+csrf_token;
        SendRequest("POST", URL+'/terminal/log', arg, Handler); // URL описан index.html
    }

    function printToTerminal(text) {  // Вывод в терминал
        lines = terminal.value.split('\n');
        len = lines.length-100;
        if (len>0) {
            lines.splice(0,len);
        }
        
        terminal.value = lines.join('\r\n') + text.trim() + '\r\n';
        //SendToDB(text.trim())
        //alert(terminal.scrollTop);
        terminal.scrollTop = terminal.scrollHeight - terminal.clientHeight;
    }

    function subscribe_click(e, elem){ // Обработчик нажатия кнопки подписки
        e.preventDefault();
        e.stopPropagation();
        subscribe(elem);
    }

    function send_click(e, elem){ // Обработчик нажатия кнопки отправить
        e.preventDefault();
        e.stopPropagation();
        send(elem);
    }

    function unsubscribe_click(e, elem){ // Обработчик нажатия кнопки отписки
        e.preventDefault();
        e.stopPropagation();
        unsubscribe(elem);
    }

//    function send_msg_click(e, elem){ // Обработчик нажатия кнопки посылки сообщения
//        e.preventDefault();
//        e.stopPropagation();
//        sendMsg(msg.value);
//        #msg.value = '';
//    }

    function onBlur() {
        elements = document.querySelectorAll('input.topic');
        vals = ''
        for (elem of elements) {
            if (elem) {
                vals +='{|}' + elem.value;
            }
        }
        
        setCookie ('topics',vals);
        
        elements = document.querySelectorAll('input.msg');
        vals = ''
        for (elem of elements) {
            if (elem) {
                vals +='{|}' + elem.value;
            }
        }
        setCookie ('msgs',vals);
    }

    function setCookie(name, value, options = {}) {

      options = {
        path: '/',
        // при необходимости добавьте другие значения по умолчанию
        ...options
      };

      if (options.expires instanceof Date) {
        options.expires = options.expires.toUTCString();
      }

      let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value);

      for (let optionKey in options) {
        updatedCookie += "; " + optionKey;
        let optionValue = options[optionKey];
        if (optionValue !== true) {
          updatedCookie += "=" + optionValue;
        }
      }

      document.cookie = updatedCookie;
    }
    
    function getSister(elem,selector) { // Получаем сестринский элемент
        parent = elem.parentElement;
        el = parent.querySelector(selector);
        return el;
    }

    function onPressTopic(elem) { // Обработчик нажатия кнопок в поле тема
        if (event.key != 'Enter'){
            setEmptyErrClass(elem)
            //device_id = getDevice(elem);
            topic = getTopic(elem);
            subscribeButton = getSister(elem, 'button.subscribe_btn');
            unsubscribeButton = getSister(elem, 'button.unsubscribe_btn');
            msg = getSister(elem, 'input.msg');
            sendButton = getSister(elem, 'button.send_btn');
            if (topic.classList.contains(EMPTY)) {
                 subscribeButton.setAttribute('disabled','disabled');
                 msg.setAttribute('disabled','disabled');
                 sendButton.setAttribute('disabled','disabled');
            } else {
                 subscribeButton.removeAttribute('disabled');
                 msg.removeAttribute('disabled');
                if (msg.classList.contains(EMPTY)) {
                    sendButton.setAttribute('disabled','disabled');
                 }
            }
        } else {
            if (event.type == 'keypress') {
                subscribe_click(event,elem);
            }
        }
    }

    function onPressMsg(elem) { // Обработчик нажатия кнопок в поле сообщение
        if (event.key != 'Enter'){
            setEmptyErrClass(elem)
            //device_id = getDevice(elem);
            sendButton = getSister(elem, 'button.send_btn');
            msg = getSister(elem, 'input.msg');
            topic = getSister(elem,'input.topic');
            if (msg.classList.contains(EMPTY) || topic.classList.contains(EMPTY)) {
                 sendButton.setAttribute('disabled','disabled');
            } else {
                 sendButton.removeAttribute('disabled');
            }
        } else {
            if (event.type == 'keypress') {
                send_click(event,sendButton);
            }
        }
    }

//!!!
    function disableSendButtons(){ // Деактивация кнопок отправки
        cmdButtons = document.querySelectorAll('button.send_cmd_btn'); // Кнопки отправки комманд
        setsAttribute(cmdButtons, 'disabled','disabled');
        msgButtons = document.querySelectorAll('button.send_msg_btn');// Кнопка(и) отправки сообщений
        setsAttribute(msgButtons, 'disabled','disabled');
    }

//!!!
    function enableSendButtons(){// Активация кнопок отправки
        msgButtons = document.querySelectorAll('button.send_msg_btn');// Кнопка(и) отправки сообщений
        removesAttribute(msgButtons, 'disabled');
        // Кнопки отправки комманд активировать если заполнены соответствующие поля комманд
        cmdButtons = document.querySelectorAll('button.send_cmd_btn'); // Кнопки отправки комманд
        for (btn of cmdButtons) {
            cmd=getTopic(btn)
            val = cmd.value
            val = val.trim()
            if (val != '') {
                btn.removeAttribute('disabled');
            }
        }
    }
    
    function byteString(n) {
        if (n < 0 || n > 255 || n % 1 !== 0) {
            throw new Error(n + " does not fit in a byte");
        }
        return ("000000000" + n.toString(2)).substr(-8)
    }

    function HTMLFromFlowMeter(data){
        packetType = data.PacketType;
        flags = data.Flags;
        bits = byteString(flags);
        HTML = `Расходомер <br>`;
        HTML += ` Тип пакета: ${packetType}  Флаги: ${bits}=${flags}`;
        HTML += "<table><tr><th>Время</th><th>Показание</th></tr>";
        for (item of data.Measurings) {
            dt = item.MeasuringDateTime;
            value = item.MeasuringValueFloat;
            HTML += `<tr><td>${dt}</td><td>${value}</td></tr>`;
        }
        HTML += "</table>";
        return HTML        
    }

    function HTMLFromPressure(data){
        packetType = data.PacketType;
        flags = data.Flags;
        bits = byteString(flags);
        HTML = `Давление <br>`;
        HTML += `Тип пакета: ${packetType} Флаги: ${bits}=${flags}`;
        HTML += "<table><tr><th>Время</th><th>Показание</th></tr>";
        for (item of data.Measurings) {
            dt = item.MeasuringDateTime;
            value = item.MeasuringValueInt;
            HTML += `<tr><td>${dt}</td><td>${value}</td></tr>`;
        }
        HTML += "</table>";
        return HTML
    }
    
    function addToChart(data) {
        times = [];
        values = [];
        for (item of data.payload.Measurings) {
            times.push(item.MeasuringDateTime);
            if ('MeasuringValueFloat' in item) {
                values.push(item.MeasuringValueFloat);
            } else {
                values.push(item.MeasuringValueInt);
            }
        }
        chartConfig.data = {
            labels: times,
            datasets: [{
                label: data.topic,
                data: values,
                backgroundColor: 'rgba(255, 99, 132, 0.6)',
                borderColor: 'rgba(255, 99, 132, 0.6)'
            }]
        }
        //myChart = new Chart(popCanvas, chartConfig);
        topic = data.topic;
        topic = topic.replace(/\//g,'_');
        el = document.querySelector('.'+topic);
        if (!el) {
            el = document.querySelector('.template.chart');
        }
        if (el) {
            id = el.getAttribute('chartid');
            el.classList.remove('template');
            el.classList.add(topic);
            myChart[id].data = chartConfig.data;
            myChart[id].update();
        }
    }


    function createSocket(url){ // Создание WebSocket
        var sock = new WebSocket(url);
         sock.onopen = function() {
             console.log('socket open');
             sendMsg('User "'+USERNAME+'" connected to Terminal'); // + this.url
             //enableSendButtons();
             connect.setAttribute('disabled','disabled');
             enable_cmd_input();
             interf = document.getElementById('interface');
             interf.classList.remove('disconnect');
         };

         sock.onmessage = function(e) {
             //console.log('message', e.data);
             printToTerminal(e.data)
             data = JSON.parse(e.data)
             switch (data.msgtype){
              case 'Pressure':
                data.HTML = HTMLFromPressure(data.payload);
                addToChart(data);
                break;
              case 'FlowMeter':
                data.HTML = HTMLFromFlowMeter(data.payload);
                addToChart(data);
                break;
              default:
                data.HTML = data.payload;
                break;
             }
             table_add_row(data.datetime, data.topic, data.HTML);
         };

         sock.onclose = function() {
             console.log('socket close');
             printToTerminal('WARNING Connection closed');
             disableSendButtons();
             connect.removeAttribute('disabled');
             disable_cmd_input();
             interf = document.getElementById('interface');
             interf.classList.add('disconnect');
         };
         
         sock.onerror = function() {
             console.log('socket error');
             printToTerminal('Error Connection');
             disableSendButtons();
             connect.removeAttribute('disabled');
             interf = document.getElementById('interface');
             interf.classList.add('disconnect');
         }
         return sock;
    };

    function setsAttribute(elements, attr_name, attr_value){
        for (elem of elements) {
            elem.setAttribute(attr_name, attr_value);
        }
    }

    function removesAttribute(elements, attr_name){
        for (elem of elements) {
            elem.removeAttribute(attr_name);
        }
    }
    
    

    function subscribe(elem){ // Функция подписки на тему
        topic = getSister(elem, 'input.topic');
        message = {};
        message.topic = topic.value;
        message.api = 'subscribe';
        jsonMessage = JSON.stringify(message);
        //printToTerminal(jsonMessage);
        webSocket.send(jsonMessage);
        
        subscribeButton = getSister(elem, 'button.subscribe_btn');
        unsubscribeButton = getSister(elem, 'button.unsubscribe_btn');
        msg = getSister(elem, 'input.msg');

        subscribeButton.setAttribute('disabled','disabled');
        topic.setAttribute('disabled','disabled');
        msg.removeAttribute('disabled');
        unsubscribeButton.removeAttribute('disabled');
    }


    function send(elem){ // Функция отправки сообщения
        msg = getSister(elem, 'input.msg');
        topic = getSister(elem, 'input.topic');
        message = {};
        message.text = msg.value.replace(/\\\\/g, "\\");;
        message.topic = topic.value;
        message.api = 'payload';
        jsonMessage = JSON.stringify(message);
        //printToTerminal(jsonMessage);
        webSocket.send(jsonMessage);
        
        sendButton = getSister(elem, 'button.send_btn');

        sendButton.setAttribute('disabled','disabled');
        msg.setAttribute('disabled','disabled');
        elArr = [sendButton,msg];
        removeAttrsBind = removeAttrs.bind(elem,elArr,'disabled');
        setTimeout(removeAttrsBind,1000);
    }

    function unsubscribe(elem) {
        topic = getSister(elem, 'input.topic');
        message = {};
        message.topic = topic.value;
        if (message.topic) {
            message.api = 'unsubscribe';
            jsonMessage = JSON.stringify(message);
            //printToTerminal(jsonMessage);
            webSocket.send(jsonMessage);
        }

        subscribeButton = getSister(elem, 'button.subscribe_btn');
        unsubscribeButton = getSister(elem, 'button.unsubscribe_btn');
        msg = getSister(elem, 'input.msg');

        unsubscribeButton.setAttribute('disabled','disabled');
        topic.removeAttribute('disabled');
        msg.removeAttribute('disabled');
        subscribeButton.removeAttribute('disabled');
    }

    function sendMsg(txt){ // Функция отправки сообщения (если подключится несколько пользователей то смогут переписываьтся)
        message = {};
        message.msg = txt;
        message.user = USERNAME;
        jsonMessage = JSON.stringify(message);
        //printToTerminal(jsonMessage);
        webSocket.send(jsonMessage);
    }

    function changeLineBreak(){ // Включить/выключить перенос
        //lineBreak.checked = !lineBreak.checked;
        if (lineBreak.checked) {
            terminal.setAttribute('wrap','soft')
        } else {
            terminal.setAttribute('wrap','off')
        }
    }
    
    function table_add_row(date, topic, message){
        table = document.getElementById('table_body');
        removeCount=table.childNodes.length-100;
        if (removeCount>0) {
            for (i=0; i<removeCount; i++) {
                table.childNodes[0].remove();
            }
        }
        row = document.createElement('tr');
        row.innerHTML = `<td class='td1'>${date}</td><td class='td2'>${topic}</td><td class='td3'>${message}</td>`;
        table.append(row);
        table.scrollTop = table.scrollHeight - table.clientHeight;
        add_to_topics(topic);
    }
    
    function add_to_topics(topic){// Добваление темы в фильтр
        elem = document.getElementById('topics');
        if (!(elem.atopics.includes(topic))) {
            elem.atopics.push(topic);
            opt = document.createElement('option');
            opt.innerHTML = topic
            elem.append(opt);
        }
    }

    function copyToClipboard(textToCopy) {
        // navigator clipboard api needs a secure context (https)
        if (navigator.clipboard && window.isSecureContext) {
            // navigator clipboard api method'
            return navigator.clipboard.writeText(textToCopy);
        } else {
            // text area method
            let textArea = document.createElement("textarea");
            textArea.value = textToCopy;
            // make the textarea out of viewport
            textArea.style.position = "fixed";
            textArea.style.left = "-999999px";
            textArea.style.top = "-999999px";
            document.body.appendChild(textArea);
            textArea.focus();
            textArea.select();
            return new Promise((res, rej) => {
                // here the magic happens
                document.execCommand('copy') ? res() : rej();
                textArea.remove();
            });
        }
    }
    
    function clip_click(el) { // Таблицу как HTML в буфер обмена
        table = document.getElementById('table_div');
        str = table.innerHTML;
        copyToClipboard(str);
    }

    function clip1_click(el) { // Таблицу как текст в буфер обмена
        table = document.getElementById('table_body');
        str = table.outerText;
        copyToClipboard
    }
    
    function clear_click(el) {
        table_body = document.getElementById('table_body');
        table_body.innerHTML = '';
    }
    
    function disable_cmd_input() {
        cmds = document.getElementById('cmds_div');
        buttons = cmds.querySelectorAll('button');
        for (butn of buttons) {
            butn.setAttribute('disabled','disabled');
        }
        inputs = cmds.querySelectorAll('input');
        for (input of inputs) {
            input.setAttribute('disabled','disabled');
        }
    }

    function enable_cmd_input() {
        cmds = document.getElementById('cmds_div');
        /*buttons = cmds.querySelectorAll('button.subscribe_btn');
        removeAttrs(buttons, 'disabled');
        buttons = cmds.querySelectorAll('button.send_btn');
        removeAttrs(buttons, 'disabled');*/
        buttons = cmds.querySelectorAll('button.del_subscribe_btn');
        if (buttons.length>2) {
            removeAttrs(buttons, 'disabled');
        }
        btn = document.getElementById('add_subscribe_btn');
        btn.removeAttribute('disabled');

        inputs = cmds.querySelectorAll('input.topic');
        removeAttrs(inputs, 'disabled');
        for (inp of inputs) {
            onPressTopic(inp);
        }
        inputs = cmds.querySelectorAll('input.msg');
        removeAttrs(inputs, 'disabled');
        for (inp of inputs) {
            onPressMsg(inp);
        }
    }
    
    function removeAttrs(elements_array, attr) {
        for (el of elements_array) {
            el.removeAttribute(attr);
        }
    }
    
    function setAttrs(elements_array, attr, val) {
        for (el of elements_array) {
            el.setAttribute(attr, val);
        }
    }
